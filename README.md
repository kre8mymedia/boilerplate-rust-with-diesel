# Rust Postgres Diesel Demo

### Start Postgres/PGAdmin
<pre>
cd diesel_demo
docker-compose up --build
</pre>

### Seed some posts in the database
<pre>
cargo run --bin write_post
</pre>

### Show posts in the database
<pre>
cargo run --bin show_posts
</pre>