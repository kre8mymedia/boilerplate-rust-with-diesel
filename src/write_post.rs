extern crate diesel;
extern crate diesel_demo;

use self::diesel_demo::*;

fn main() {
    let connection = establish_connection();

    let number = 10; 
    for num in 0..number { // change it to get range
        let title = format!("{}{}", "Title", num);
        let body = format!("{}{}", "This is the body of post ", num);
        let post = create_post(&connection, &title, &body);
        println!("\nSaved draft {} with id {}", title, post.id);
    }
}
